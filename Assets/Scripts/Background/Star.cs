﻿using UnityEngine;

public class Star : MonoBehaviour
{
    [Range(.2f, .4f)]
    public float startSizeMin;

    [Range(.2f, .4f)]
    public float startSizeMax;

    [Range(0f, 360f)]
    public float startRot;
    [Space]
    public float rotSpeed = 7f;

    private float parentWidth;
    private float parentHeight;

    private void Awake()
    {
        parentWidth = this.transform.lossyScale.x;
        parentHeight = this.transform.lossyScale.y;
    }

    private void Start()
    {
        var size = Random.Range(startSizeMin, startSizeMax);

        this.transform.localScale = new Vector3(size/(2*parentWidth), size/(2*parentHeight), 1f);
        this.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, startRot));
    }

    private void Update()
    {
        this.transform.Rotate(new Vector3(0f, 0f, rotSpeed * Time.deltaTime));
    }
}
