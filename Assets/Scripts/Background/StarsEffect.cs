﻿using UnityEngine;

public class StarsEffect : MonoBehaviour
{
    [Header("Background Effects")]
    public GameObject star;

    private float width;
    private float height;

    [Range(20,100)]
    public int starsToSpawn;

    private void Start()
    {
        height = Camera.main.orthographicSize * 2;
        width = Camera.main.aspect * height;

        CreateStars();
    }

    public void CreateStars()
    {
        for (int i = 0; i < starsToSpawn; i++)
        {
            var pos = new Vector2(Random.Range(-width, width), Random.Range(-height, height));
            Instantiate(star, pos, Quaternion.identity, this.transform);
        }
    }
}
