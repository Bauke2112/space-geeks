﻿using UnityEngine;

/* 
    This scriptable object represents a bool
    It is used to develop games using a scriptable object data structure
*/

[CreateAssetMenu(fileName = "Bool Variable", menuName = "Data Strcutures/Bool")]
public class BoolVariable : ScriptableObject
{
    public bool value;
}

