﻿using UnityEngine;

/* 
    This scriptable object represents a float
    It is used to develop games using a scriptable object data structure
*/

[CreateAssetMenu(fileName = "Float Variable", menuName = "Data Strcutures/Float")]
public class FloatVariable : ScriptableObject
{
    public float value;
}
