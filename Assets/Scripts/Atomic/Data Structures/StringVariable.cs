﻿using UnityEngine;

/* 
    This scriptable object represents a string
    It is used to develop games using a scriptable object data structure
*/

[CreateAssetMenu(fileName = "String Variable", menuName = "Data Strcutures/String")]
public class StringVariable : ScriptableObject
{
    public string value;
}

