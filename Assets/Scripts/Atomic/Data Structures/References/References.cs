﻿using UnityEngine;

[System.Serializable]
public class Float //Scriptable Object data archtature float
{
    public bool useConstant = false;
    public FloatVariable variable;
    public float constant;

    public float value
    {
        get { return useConstant ? constant : variable.value;  }
        set
        {
            if (useConstant)
                constant = value; //constant value
            else
                variable.value = value; //scriptable object variable
        }
    }
}

[System.Serializable]
public class Int //Scriptable Object data archtature int
{
    public bool useConstant = false;
    public IntVariable variable;
    public int constant;

    public int value
    {
        get { return useConstant ? constant : variable.value; }
        set
        {
            if (useConstant)
                constant = value; //constant value
            else
                variable.value = value; //scriptable object variable
        }
    }
}

[System.Serializable]
public class Bool //Scriptable Object data archtature bool
{
    public bool useConstant = false;
    public BoolVariable variable;
    public bool constant;

    public bool value
    {
        get { return useConstant ? constant : variable.value; }
        set
        {
            if (useConstant)
                constant = value; //constant value
            else
                variable.value = value; //scriptable object variable
        }
    }
}

[System.Serializable]
public class String //Scriptable Object data archtature string
{
    public bool useConstant = false;
    public StringVariable variable;
    public string constant;

    public string value
    {
        get { return useConstant ? constant : variable.value; }
        set
        {
            if (useConstant)
                constant = value; //constant value
            else
                variable.value = value; //scriptable object variable
        }
    }
}
