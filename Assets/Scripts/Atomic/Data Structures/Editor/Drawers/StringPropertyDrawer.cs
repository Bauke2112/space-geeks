﻿using UnityEngine;
using UnityEditor;
using System.Linq;

[CustomPropertyDrawer(typeof(String))]
public class StringPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        bool useConstant = property.FindPropertyRelative("useConstant").boolValue;

        //Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        var rect = new Rect(position.position, Vector2.one * 20);

        if (EditorGUI.DropdownButton(rect, new GUIContent(GetTexture()), FocusType.Keyboard, new GUIStyle()
        {
            fixedWidth = 50f,
            border = new RectOffset(1, 1, 1, 1)
        }))
        {
            GenericMenu menu = new GenericMenu();
            menu.AddItem(new GUIContent("Constant"), useConstant, () => SetProperty(property, true));
            menu.AddItem(new GUIContent("Variable"), !useConstant, () => SetProperty(property, false));

            menu.ShowAsContext();
        }

        position.position += Vector2.right * 15;
        string value = property.FindPropertyRelative("constant").stringValue;

        if (useConstant)
        {
            string newValue = EditorGUI.TextField(position, value.ToString());
            property.FindPropertyRelative("constant").stringValue = value;
        }
        else
        {
            EditorGUI.ObjectField(position, property.FindPropertyRelative("variable"), GUIContent.none);
        }

        EditorGUI.EndProperty();
    }

    private void SetProperty(SerializedProperty property, bool value)
    {
        var propRelative = property.FindPropertyRelative("useConstant");
        propRelative.boolValue = value;
        property.serializedObject.ApplyModifiedProperties();
    }

    private Texture GetTexture()
    {
        var textures = Resources.FindObjectsOfTypeAll(typeof(Texture))
            .Where(t => t.name.ToLower().Contains("square"))
            .Cast<Texture>().ToList();
        return textures[0];
    }
}