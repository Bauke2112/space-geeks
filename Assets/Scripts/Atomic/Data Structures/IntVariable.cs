﻿using UnityEngine;

/* 
    This scriptable object represents a integer
    It is used to develop games using a scriptable object data structure
*/

[CreateAssetMenu(fileName = "Int Variable", menuName = "Data Strcutures/Int")]
public class IntVariable : ScriptableObject
{
    public int value;
}
