﻿using UnityEngine;

namespace Atomic
{
    namespace Events
    {
        //This class manages the association between Custom Events and Subscribers
        public class EventManagerInstance : MonoBehaviour
        {
            //This function runs only when the events list is updated at the Event Manager Static Script
            public void HandleEvent(CustomEvent _e)
            {
                foreach (CustomEvent _event in EventManager.events) //Loop through all published events at the events list
                {
                    if (_event.name == _e.name) //Compare event parameter name with each element of the events list
                    {
                        var eventObject = EventManager.subscribers.FindValue(EventManager.subscribers.FindKey(_e.name)).GetComponent<IEventObject>(); //Get the component that recieves the event in the subscribed object
                        eventObject.OnEventReceived(_event); //Calls the OnReceivedEvent function on the subscribed object

                        EventManager.RemoveEvent(_event); //Remove event from the events list
                        break; //breaks the foreach loop
                    }
                }
            }
        }

    }
}
