﻿namespace Atomic
{
    namespace Events
    {
        public interface IDataEventObject
        {
            void OnEventReceived(CustomEvent _e);
        }
    }
}
