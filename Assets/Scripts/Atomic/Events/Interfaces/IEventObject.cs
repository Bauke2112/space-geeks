﻿namespace Atomic
{
    namespace Events
    {
        public interface IEventObject //Interface that is mandatory to components be able to Receive Events
        {
            void OnEventReceived(CustomEvent _e); //Called when the subscriber exists in the list and a event is invoked
        }
    }
}
