﻿using UnityEngine;

namespace Atomic
{
    namespace Events
    {
        public class CustomEvent //Custom Event class
        {
            public string name; //event name

            //Custom event constructor
            public CustomEvent(string _name)
            {
                name = _name;
            }
        }

        public class DataEvent<dataType> : CustomEvent //Events with data
        {
            private dataType data; //Stored data

            //Event Data constructor
            public DataEvent(string _name, dataType _data) : base(_name)
            {
                data = _data;
            }

            public dataType GetData() //Get data
            {
                return this.data;
            }
        }
    }
}
