﻿using System.Linq;
using System.Collections.Generic;

public class AssociationList<keyType, valueType>
{
    public List<keyType> keys
    {
        get;
        private set;
    } = new List<keyType>();
    public List<valueType> values
    {
        get;
        private set;
    } = new List<valueType>();

    //Methods
    public List<keyType> Keys()
    {
        return keys.ToList();
    }

    public keyType FindKey(keyType key)
    {
        for (int i = 0; i < keys.Count; i++)
        {
            if(keys[i].Equals(key))
            {
                return keys[i];
            }
        }

        return default;
    }

    public int FindKeyIndex(keyType key)
    {
        for (int i = 0; i < keys.Count; i++)
        {
            if (keys[i].Equals(key))
            {
                return i;
            }
        }

        return default;
    }

    public List<valueType> Values()
    {
        return values.ToList();
    }

    public valueType FindValue(keyType key)
    {
        for (int i = 0; i < keys.Count; i++)
        {
            if (keys[i].Equals(key))
            {
                return values[i];
            }
        }

        return default;
    }

    public int FindValueIndex(keyType key)
    {
        for (int i = 0; i < keys.Count; i++)
        {
            if (keys[i].Equals(key))
            {
                return i;
            }
        }

        return default;
    }

    public void Add(keyType key, valueType value)
    {
        keys.Add(key);
        values.Add(value);
    }

    public void Remove(int keyIndex)
    {
        keys.Remove(keys[keyIndex]);
        values.Remove(values[keyIndex]);
    }

    public void Clear()
    {
        for(int i = 0; i < keys.Count; i++)
        {
            keys.Remove(keys[i]);
            values.Remove(values[i]);
            i--;
        }
    }
}
