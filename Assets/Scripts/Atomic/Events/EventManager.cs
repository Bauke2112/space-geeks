﻿using UnityEngine;
using System.Collections.Generic;

namespace Atomic
{
    namespace Events
    {
        public static class EventManager
        {
            public static List<CustomEvent> events = new List<CustomEvent>(); //events list
            public static AssociationList<string, Component> subscribers = new AssociationList<string, Component>();

            public static EventManagerInstance manager = GameObject.FindObjectOfType<EventManagerInstance>(); //Instantiable Event Manager instance

            static Component c; //Used in Publish Event function. It is just a component instance

            #region CustomEvents Methods
            public static void PublishEvent(CustomEvent _event) //Adds events to the event list
            {
                foreach (CustomEvent e in events) //Loop to verify if event is already published
                {
                    if (e == _event) events.Remove(e); //Removes the event if it already exists
                    break; //breaks the loop
                }

                events.Add(_event); //Add event to event list

                if (manager == null) return; //Checks if manager is already assigned

                manager.HandleEvent(_event); //Calls HandleEvent function at EventManagerInstance 
            }

            public static void RemoveEvent(CustomEvent _event) //Remove event from list
            {
                if (events == null) return; //check if list is null

                events.Remove(_event); //remove from list
            }

            public static void SubscribeToEvent(string _eventName, Component subscriber) //Subscribe component to event
            {
                subscribers.Add(_eventName, subscriber);

                if (manager == null) //Checks if manager variable is null
                {
                    var m = new GameObject("Event Manager"); //Creates new game object
                    m.AddComponent(typeof(EventManagerInstance)); //Adds EventManagerInstance to the created game object
                    manager = m.GetComponent<EventManagerInstance>(); //assigns the craated manager to the manager variable
                }
            }

            public static void Unsubscribe(string eventName)
            {
                if(subscribers.FindKey(eventName) != "")
                {
                    subscribers.Remove(subscribers.FindKeyIndex(eventName));
                }
            }
            #endregion
        }
    }
}

#region Comments
/* 
public static class EventManager
        {
            public static List<CustomEvent> events = new List<CustomEvent>(); //events list
            public static AssociationList<string, Component> subscribers = new AssociationList<string, Component>();

            public static EventManagerInstance manager = GameObject.FindObjectOfType<EventManagerInstance>(); //Instantiable Event Manager instance

            static Component c; //Used in Publish Event function. It is just a component instance
            public static void PublishEvent(CustomEvent _event) //Adds events to the event list
            {
                foreach (CustomEvent e in events) //Loop to verify if event is already published
                {
                    if (e == _event) events.Remove(e); //Removes the event if it already exists
                    break; //breaks the loop
                }

                events.Add(_event); //Add event to event list

                if (manager == null) return; //Checks if manager is already assigned

                manager.HandleEvent(_event); //Calls HandleEvent function at EventManagerInstance 
            }

            public static void RemoveEvent(CustomEvent _event) //Remove event from list
            {
                if (events == null) return; //check if list is null

                events.Remove(_event); //remove from list
            }

            public static void SubscribeToEvent(string _eventName, Component subscriber) //Subscribe component to event
            {
                subscribers.Add(_eventName, subscriber);

                if (manager == null) //Checks if manager variable is null
                {
                    var m = new GameObject("Event Manager"); //Creates new game object
                    m.AddComponent(typeof(EventManagerInstance)); //Adds EventManagerInstance to the created game object
                    manager = m.GetComponent<EventManagerInstance>(); //assigns the craated manager to the manager variable
                }
            }
        } 
*/
#endregion
