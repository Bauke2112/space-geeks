﻿using UnityEngine;
using Effects;

[RequireComponent(typeof(SpriteRenderer))]
public class Transition : MonoBehaviour
{
    public static Transition transition;

    private void Awake()
    {
        if(transition == null) transition = this;
        else
        {
            Destroy(transition.gameObject);
            transition = this;
        }
    }

    public float timeToFade;
    public SpriteRenderer rend;

    private void OnEnable()
    {
        rend.color = new Color(rend.color.r, rend.color.g, rend.color.b, 1f);
        StartCoroutine(rend.DecreaseAlpha(timeToFade, 0f));
    }

    public void FadeIn(CustomAction action, string sceneName)
    {
        rend.color = new Color(0f, 0f, 0f, 0f);
        StartCoroutine(rend.IncreaseAlpha(timeToFade, 1f, action, sceneName));
    }
}
