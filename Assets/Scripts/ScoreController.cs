﻿using UnityEngine;
using Atomic.Events;
using Timers;

public class ScoreController : MonoBehaviour, IEventObject{
    public Int sceneScore;

    public int asteroidScore;
    public int enemyScore;
    public int powerUpScore;

    private void Start() {
        sceneScore.value = 0;
        EventManager.SubscribeToEvent("Enemy Destroyed", this);
        EventManager.SubscribeToEvent("Asteroid Destroyed", this);
        EventManager.SubscribeToEvent("Power Up", this);

        if(PlayerPrefs.GetInt("high-score") < 0)
            PlayerPrefs.SetInt("high-score", -100000000);

        Timer.CreateTimer(() =>
        {
            Timer.CreateTimer(() =>
            {
                SaveScore();
                SaveHighScore();
            }, 2f);
        }, 2f);
    }

    private void OnDisable()
    {
        EventManager.Unsubscribe("Enemy Destroyed");
        EventManager.Unsubscribe("Asteroid Destroyed");
        EventManager.Unsubscribe("Power Up");
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.L))
        {
            sceneScore.value += (Mathf.RoundToInt(Mathf.Log(Mathf.Log10(439), 2) * (439))) * 5;
        }
    }

    public void SaveScore() {
        PlayerPrefs.SetInt("score", sceneScore.value);
    }

    public void SaveHighScore() {
        if(sceneScore.value > PlayerPrefs.GetInt("high-score")) {
            PlayerPrefs.SetInt("high-score", sceneScore.value);
        }else return;
    }

    public void UpdateScore(string updateType)
    {
        switch(updateType)
        {
            case "Enemy Destroyed":
                sceneScore.value += enemyScore;
                break;
            case "Asteroid Destroyed":
                sceneScore.value += asteroidScore;
                break;
            case "Power Up":
                sceneScore.value += powerUpScore;
                break;
        }
    }

    public void OnEventReceived(CustomEvent _e)
    {
        foreach (string s in EventManager.subscribers.keys)
        {
            if (s == _e.name && EventManager.subscribers.FindValue(s) == this)
            {
                UpdateScore(_e.name);
                break;
            }
        }
    }
}
