﻿using UnityEngine;

public static class ComponentsUtilities
{
    public static void SetActive(this Component t, bool value)
    {
        t.gameObject.SetActive(value);
    }

    public static void DestroyObject(this Component t)
    {
        GameObject.Destroy(t.gameObject);
    }

    public static void RemoveComponent(this Component t, Component toRemove)
    {
        GameObject.Destroy(t.GetComponent(toRemove.GetType()));
    }

    public static string GetTag(this Component t)
    {
        return t.gameObject.tag;
    }

    public static void SetTag(this Component t, string tag)
    {
        t.gameObject.tag = tag;
    }

    public static void SetLayerMask(this Component t, string layerName)
    {
        t.gameObject.layer = LayerMask.NameToLayer(layerName);
    }

    public static string GetLayerMask(this Component t)
    {
        return t.gameObject.GetComponent<SpriteRenderer>().sortingLayerName;
    }

    public static Transform GetParentTransform(this Component t)
    {
        return t.transform.parent;
    }

    public static void SetParentTransform(this Component t, Transform p)
    {
        t.transform.SetParent(p);
    }

    //Transform Utilities
    #region Tranform Extensions
    public static Vector2 SetDirection(this Transform t, Vector3 targetPosition)
    {
        return (t.position - targetPosition).normalized;
    }

    public static Vector2 SetDirection(this Transform t, Transform target)
    {
        return (t.position - target.position).normalized;
    }

    public static void LookAt2D(this Transform t, Transform target, float correction)
    {
        var dir = (t.position - target.position).normalized;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + correction;
        t.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle));
    }

    public static void LookAt2D(this Transform t, Vector3 target, float correction) {
        var dir = (t.position - target).normalized;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + correction;
        t.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle));
    }

    public static void LookAtMouse2D(this Transform t, float correction, bool inverted = false)
    {
        Vector3 mousePos;

        if (!inverted)
        {
            mousePos = Input.mousePosition;
            var dir = (t.position - mousePos).normalized;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + correction;
            t.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle));
        }
        else
        {
            mousePos = new Vector3(Input.mousePosition.x, -Input.mousePosition.y);
            var dir = (t.position - mousePos).normalized;
            var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg + correction;
            t.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle));
        }
    }
    #endregion

    //Rigidbody Utilities
    #region Rigidbody2D
    public static void Reset(this Rigidbody2D rb)
    {
        rb.velocity = Vector2.zero;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
    }
    #endregion
}
