﻿using UnityEngine.SceneManagement;
using UnityEngine;

namespace Buttons
{
    public static class ButtonsUtilities
    {
        public static void LoadScene(string sceneName)
        {
            SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
        }

        public static void LoadScene(int sceneID)
        {
            SceneManager.LoadScene(sceneID, LoadSceneMode.Single);
        }

        public static void LoadScene(Scene scene)
        {
            SceneManager.LoadScene(scene.buildIndex, LoadSceneMode.Single);
        }

        public static void LoadLastScene()
        {
            Scene curScene = SceneManager.GetActiveScene();
            int idToLoad = curScene.buildIndex - 1;

            LoadScene(idToLoad);
        }

        public static void LoadNextScene()
        {
            Scene curScene = SceneManager.GetActiveScene();
            int idToLoad = curScene.buildIndex + 1;

            LoadScene(idToLoad);
        }

        public static void Exit()
        {
            Application.Quit();
        }
    }
}
