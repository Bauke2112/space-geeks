﻿using UnityEngine;

namespace TimeUtils
{
    public static class TimeUtilities
    {
        public static readonly float defaultTime = 1f;
        public static readonly float defaultHalfTime = .5f;
        public static readonly float deafaultDoubleTime = 2f;

        public static bool isTimePaused;

        //Time Utilities
        #region Time Utilities
        public static void PauseTime()
        {
            Time.timeScale = 0;
            isTimePaused = true;
        }

        public static void ResetTime()
        {
            Time.timeScale = defaultTime;
            isTimePaused = false;
        }

        public static void HalfTime()
        {
            Time.timeScale = defaultHalfTime;
            isTimePaused = false;
        }

        public static void DoubleTime()
        {
            Time.timeScale = deafaultDoubleTime;
            isTimePaused = false;
        }
        #endregion
    }
}
