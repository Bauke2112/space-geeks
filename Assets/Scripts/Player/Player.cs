﻿using UnityEngine;

public class Player : MonoBehaviour
{
    [Header("Player Config")]
    public int playerNumber;

    [Header("Player Variables")]
    public float maxSpeed;
    public float rotSpeed;
    public float acc;
    //[Space]
    //public float fireRate;

    [Header("Key Maps")]
    public string lockRotationKey = "button_4_joy_";
    public string lockMovementKey = "button_1_joy_";
    public string shootKey = "button_3_joy_";
    public string pauseKey = "button_9_joy_";
    public string weaponA = "button_7_joy_";
    public string weaponB = "button_8_joy_";
    
    //[Header("Damage")]
    //public float startDamage;
    //public float damage {get; set;}
    
    public bool canShoot { get; set; } = true;
    [Space]
    public float safeZoneRadius;

    private void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, safeZoneRadius);    
    }

    //void Start() {
    //    this.damage = startDamage;
    //}
}
