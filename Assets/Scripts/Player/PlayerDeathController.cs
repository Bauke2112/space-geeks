﻿using UnityEngine;
using Buttons;

public class PlayerDeathController : MonoBehaviour
{
    public static PlayerDeathController controller;

    private void Awake()
    {
        if (controller == null) controller = this;
        else
        {
            Destroy(controller.gameObject);
            controller = this;
        }
    }

    public bool oneHasDied, twoHasDied;
    public Player one, two;

    private void Update()
    {
        if (oneHasDied) one.SetActive(false);
        if (twoHasDied) two.SetActive(false);

        if(oneHasDied && twoHasDied)
        {
            Transition.transition.FadeIn(ButtonsUtilities.LoadScene, "GameOver");
        }
    }
}
