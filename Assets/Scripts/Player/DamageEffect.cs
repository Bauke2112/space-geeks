﻿using UnityEngine;

public class DamageEffect : MonoBehaviour
{
    private SpriteRenderer rend;
    private CharacterStats stats;

    public Sprite[] effects;

    void Awake()
    {
        rend = this.GetComponent<SpriteRenderer>();
        stats = GetComponentInParent<CharacterStats>();
    }

    void Update()
    {
        if(stats.lives == 4)
            rend.sprite = effects[0];
        else if(stats.lives == 2)
            rend.sprite = effects[1];
        else if(stats.lives == 1)
            rend.sprite = effects[2];
        else if(stats.lives == 6)
            rend.sprite = null;
    }
}
