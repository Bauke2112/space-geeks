﻿using UnityEngine;

public class PlayerMovement : Player
{
    private Rigidbody2D rb;
    private bool canRotate = true;
    private bool canMove = true;

    private void Awake()
    {
        rb = this.GetComponent<Rigidbody2D>();

        if (rb == null)
            return;
    }

    private void Update()
    {
        /*if (Input.GetButtonDown(this.lockRotationKey + this.playerNumber))
        {
            canRotate = !canRotate;
            transform.rotation = transform.rotation;
        }

        if (Input.GetButtonDown(this.lockMovementKey + this.playerNumber))
        {
            canMove = !canMove;
            rb.velocity = Vector2.zero;
            rb.angularVelocity = 0;
        }*/
    }

    private void FixedUpdate()
    {
        Rotate(this.transform, HorizontalInput(this.playerNumber) * this.rotSpeed);
        MoveForward(VerticalInput(this.playerNumber) * this.acc);

        ClampVelocity();
    }

    private void ClampVelocity()
    {
        if (rb.velocity.x > maxSpeed)
            rb.velocity = new Vector2(maxSpeed, rb.velocity.y);
        else if (rb.velocity.x < -maxSpeed)
            rb.velocity = new Vector2(-maxSpeed, rb.velocity.y);

        if (rb.velocity.y > maxSpeed)
            rb.velocity = new Vector2(rb.velocity.x, maxSpeed);
        else if (rb.velocity.y < -maxSpeed)
            rb.velocity = new Vector2(rb.velocity.x, -maxSpeed);
    }

    private void MoveForward(float _acc)
    {
        if (_acc <= .1f && _acc >= -.1f)
            return;

        if (canMove == false)
            return;

        Vector2 force = transform.up * _acc * this.rb.mass;
        rb.AddForce(force);
    }

    private void Rotate(Transform t, float speed)
    {
        if (canRotate == false)
            return;

        t.Rotate(new Vector3(0f, 0f, -speed));
    }

    private float HorizontalInput(int playerNumber)
    {
        float x = Input.GetAxis("Horizontal_" + playerNumber);
        return x;
    }

    private float VerticalInput(int playerNumber)
    {
        float y = Input.GetAxis("Vertical_" + playerNumber);
        return y;
    }
}
