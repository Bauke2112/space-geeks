﻿using UnityEngine;

public class AssetsHandler : MonoBehaviour
{
    public static AssetsHandler i;

    public void Awake()
    {
        i = this;
    }


    [Header("Bullets")]
    public GameObject playerBullet_one;
    public GameObject playerBullet_two;
    public GameObject explosion;

    [Header("Asteroids")]
    public GameObject bigAsteroid;
    public GameObject mediumAsteroid;
    public GameObject smallAsteroid;
    [Space]
    public GameObject destructionExplosion;

    [Header("Enemies")]
    public GameObject[] enemies;
    public GameObject enemyBullet;

    [Header("HUD")]
    public GameObject pauseMenu;

    //Player References
    public Player[] players;

    [Header("Power Ups")]
    public GameObject powerUpItem;

    [Header("Sounds")]
    public AudioClip bullet;

    [Header("Weapons")]
    public Weapon[] weapons;
    public Weapon defaultWeapon;
    private void Start()
    {
        players = FindObjectsOfType<Player>();
    }
}
