﻿using UnityEngine;
using Timers;

public class WeaponsRequirements : MonoBehaviour
{
    public int points = 250;
    public Int score;

    private void Update()
    {
        CheckPoints();
    }

    private void CheckPoints()
    {

        var player = AssetsHandler.i.players[0].GetComponent<WeaponController>();
        var player2 = AssetsHandler.i.players[1].GetComponent<WeaponController>();
        if (score.value >= points && !player.atualWeapon.isLastWeapon)
        {
            Time.timeScale = .2f;
            //WeaponGlobal.startDeltaTime = Time.fixedDeltaTime;
            //Time.fixedDeltaTime = .002f;
            player.selector.SetActive(true);
            player2.selector.SetActive(true);

            player.weaponIconOne.sprite = player.GetNextWeaponsSprite()[0];
            player.weaponIconTwo.sprite = player.GetNextWeaponsSprite()[1];

            player2.weaponIconOne.sprite = player2.GetNextWeaponsSprite()[0];
            player2.weaponIconTwo.sprite = player2.GetNextWeaponsSprite()[1];

            if (!player.gameObject.activeInHierarchy)
                Timer.ForceTimerToZero("Player 1 Respawn Timer");

            if (!player2.gameObject.activeInHierarchy)
                Timer.ForceTimerToZero("Player 2 Respawn Timer");

            WeaponGlobal.active = true;

            points += (Mathf.RoundToInt(Mathf.Log(Mathf.Log10(points), 2)*(points/8))) * 5;
            Debug.Log(points);
        }
    }
}
