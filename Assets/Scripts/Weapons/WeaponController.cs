﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Timers;

public enum WeaponType{
    defaultWeapon = 0,
        laser = 1,
            laserForte = 2,
            metralhadoraLaser = 3,
        granadeiro = 4,
            torreI = 5,
            phantomBullet = 6
}

public class WeaponController : MonoBehaviour
{
    private Weapon defaultWeapon;
    [SerializeField]
    private WeaponType curWeapon;                                                     //Enum
    //[SerializeField]
    public Weapon atualWeapon;                                                        //Scriptable Object
    private Weapon[] pWeapons;
    private Player player;

    public GameObject selector;
    public Image weaponIconOne;
    public Image weaponIconTwo;

    public Image[] weaponImages;

    public Vector3 offset;

    int[] theOneThatWillShoot = new int[2] { 0, 0};

    private void Awake() {
        player = GetComponent<Player>();
    }
    
    private void Start() {    
        defaultWeapon = AssetsHandler.i.defaultWeapon;
        pWeapons = AssetsHandler.i.weapons;
        curWeapon = defaultWeapon.type;
        SetNewWeapon(curWeapon);
    }
       
    private void Update()
    {
        Shoot();
        
        if(selector.activeInHierarchy)
        {
            if(Input.GetButtonDown(player.weaponA + player.playerNumber))
            {
                ChangeWeapon(true);
            }else if (Input.GetButtonDown(player.weaponB + player.playerNumber))
            {
                ChangeWeapon(false);
            }
        }
    }

    public void ChangeWeapon(bool right) 
    {
        if (!atualWeapon.isLastWeapon) {
            if(right)
            {
                weaponImages[0].sprite = GetNextWeaponsSprite()[0];
                weaponImages[0].color = new Color(weaponImages[0].color.r, weaponImages[0].color.g, weaponImages[0].color.b, 1);
                weaponImages[1].sprite = GetNextWeaponsSprite()[0];
                weaponImages[1].color = new Color(weaponImages[1].color.r, weaponImages[1].color.g, weaponImages[1].color.b, 1);
                curWeapon = atualWeapon.nextWeapons[1];
                SetNewWeapon(curWeapon);                
            }
            else
            {
                weaponImages[0].sprite = GetNextWeaponsSprite()[1];
                weaponImages[0].color = new Color(weaponImages[0].color.r, weaponImages[0].color.g, weaponImages[0].color.b, 1);
                weaponImages[1].sprite = GetNextWeaponsSprite()[1];
                weaponImages[1].color = new Color(weaponImages[1].color.r, weaponImages[1].color.g, weaponImages[1].color.b, 1);
                curWeapon = atualWeapon.nextWeapons[0];
                SetNewWeapon(curWeapon);
            }

            if (player.playerNumber == 1) {
                WeaponGlobal.one = true;

                if (!AssetsHandler.i.players[1].gameObject.activeInHierarchy) WeaponGlobal.two = true;
            } else if (player.playerNumber == 2) 
            {
                WeaponGlobal.two = true;
                if (!AssetsHandler.i.players[0].gameObject.activeInHierarchy) WeaponGlobal.one = true;
            }
            selector.SetActive(false);
            
            if(WeaponGlobal.one && WeaponGlobal.two)
            {
                Time.timeScale = 1f;
                WeaponGlobal.one = false;
                WeaponGlobal.two = false;
                WeaponGlobal.active = false;
                //Time.fixedDeltaTime = WeaponGlobal.startFixedDeltaTime;
            }
        }
    }

    public void SetNewWeapon(WeaponType newWeapon)
    {
        atualWeapon = pWeapons[(int)newWeapon];
        //player.damage = atualWeapon.baseDamage;
    }
    
    public Sprite[] GetNextWeaponsSprite()
    {
        
        List<Weapon> tmp = new List<Weapon>();

        tmp.Add(pWeapons[(int)atualWeapon.nextWeapons[0]]);
        tmp.Add(pWeapons[(int)atualWeapon.nextWeapons[1]]);
        
        /**
        foreach (Weapon w in pWeapons)
        {
            if(w.type == atualWeapon.nextWeapons[0] || 
               w.type == atualWeapon.nextWeapons[1])
            {
                tmp.Add(w);
            }
        }
    */
        return new Sprite[]
        {
            tmp[1].weaponSprite,
            tmp[0].weaponSprite
        };
        
    }

    private void Shoot() {
        if (Input.GetButton(player.shootKey + player.playerNumber) && player.canShoot) {
            Bullet.player = GetComponent<Player>();
            player.canShoot = false;
            Timer.CreateTimer(() => player.canShoot = true, atualWeapon.fireRate);

            if (player.playerNumber == 1) {
                if (curWeapon == WeaponType.defaultWeapon) {
                    GameObject b = Instantiate(atualWeapon.bullet, transform.position, transform.rotation);
                    b.GetComponent<Bullet>().p = GetComponent<Player>();
                    b.GetComponent<Bullet>().damage = atualWeapon.damage;
                    b.GetComponent<Bullet>().energy = atualWeapon.energy;
                } else if(curWeapon != WeaponType.torreI) {
                    GameObject b = Instantiate(atualWeapon.bullet, weaponImages[theOneThatWillShoot[0]].transform.position + offset, transform.rotation);
                    b.GetComponent<Bullet>().p = GetComponent<Player>();
                    b.GetComponent<Bullet>().damage = atualWeapon.damage;
                    b.GetComponent<Bullet>().energy = atualWeapon.energy;
                    if (theOneThatWillShoot[0] == 0)
                        theOneThatWillShoot[0] = 1;
                    else
                        theOneThatWillShoot[0] = 0;
                } else
                {
                    GameObject a = Instantiate(atualWeapon.bullet, weaponImages[theOneThatWillShoot[0]].transform.position + offset, transform.rotation);
                    GameObject b = Instantiate(atualWeapon.bullet, weaponImages[theOneThatWillShoot[0]].transform.position + offset, transform.rotation);
                    GameObject c = Instantiate(atualWeapon.bullet, weaponImages[theOneThatWillShoot[0]].transform.position + offset, transform.rotation);

                    a.GetComponent<Bullet>().p = GetComponent<Player>();
                    a.GetComponent<Bullet>().damage = atualWeapon.damage;
                    a.GetComponent<Bullet>().energy = atualWeapon.energy;
                    b.GetComponent<Bullet>().p = GetComponent<Player>();
                    b.GetComponent<Bullet>().damage = atualWeapon.damage;
                    b.GetComponent<Bullet>().energy = atualWeapon.energy;
                    c.GetComponent<Bullet>().p = GetComponent<Player>();
                    c.GetComponent<Bullet>().damage = atualWeapon.damage;
                    c.GetComponent<Bullet>().energy = atualWeapon.energy;

                    b.transform.Rotate(new Vector3(0f, 0f, 45f));
                    c.transform.Rotate(new Vector3(0f, 0f, -45f));

                    if (theOneThatWillShoot[0] == 0)
                        theOneThatWillShoot[0] = 1;
                    else
                        theOneThatWillShoot[0] = 0;
                }
                
            } else {
                if (curWeapon == WeaponType.defaultWeapon) {
                    GameObject b = Instantiate(atualWeapon.bulletVariant, transform.position, transform.rotation);
                    b.GetComponent<Bullet>().p = GetComponent<Player>();
                    b.GetComponent<Bullet>().damage = atualWeapon.damage;
                    b.GetComponent<Bullet>().energy = atualWeapon.energy;
                } else if(curWeapon != WeaponType.torreI){
                    GameObject b = Instantiate(atualWeapon.bulletVariant, weaponImages[theOneThatWillShoot[1]].transform.position + offset, transform.rotation);
                    b.GetComponent<Bullet>().p = GetComponent<Player>();
                    b.GetComponent<Bullet>().damage = atualWeapon.damage;
                    b.GetComponent<Bullet>().energy = atualWeapon.energy;
                    if (theOneThatWillShoot[1] == 0)
                        theOneThatWillShoot[1] = 1;
                    else
                        theOneThatWillShoot[1] = 0;
                }
                else
                {
                    GameObject a = Instantiate(atualWeapon.bulletVariant, weaponImages[theOneThatWillShoot[0]].transform.position + offset, transform.rotation);
                    GameObject b = Instantiate(atualWeapon.bulletVariant, weaponImages[theOneThatWillShoot[0]].transform.position + offset, transform.rotation);
                    GameObject c = Instantiate(atualWeapon.bulletVariant, weaponImages[theOneThatWillShoot[0]].transform.position + offset, transform.rotation);

                    a.GetComponent<Bullet>().p = GetComponent<Player>();
                    a.GetComponent<Bullet>().damage = atualWeapon.damage;
                    a.GetComponent<Bullet>().energy = atualWeapon.energy;
                    b.GetComponent<Bullet>().p = GetComponent<Player>();
                    b.GetComponent<Bullet>().damage = atualWeapon.damage;
                    b.GetComponent<Bullet>().energy = atualWeapon.energy;
                    c.GetComponent<Bullet>().p = GetComponent<Player>();
                    c.GetComponent<Bullet>().damage = atualWeapon.damage;
                    c.GetComponent<Bullet>().energy = atualWeapon.energy;

                    b.transform.Rotate(new Vector3(0f, 0f, 45f));
                    c.transform.Rotate(new Vector3(0f, 0f, -45f));

                    if (theOneThatWillShoot[0] == 0)
                        theOneThatWillShoot[0] = 1;
                    else
                        theOneThatWillShoot[0] = 0;
                }
            }

        }
    }
}
