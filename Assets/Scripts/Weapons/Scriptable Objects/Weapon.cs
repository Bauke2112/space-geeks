﻿using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "Space Geeks/Weapon", order = 1)]
public class Weapon : ScriptableObject
{
    public bool isLastWeapon;
    public WeaponType type;
    public WeaponType[] nextWeapons;
    [Space]
    public Sprite weaponSprite;
    public new string name;
    [Space]
    public float damage;
    public float baseDamage;
    public float energy;
    public float fireRate;
    [Space]
    public GameObject bullet;
    public GameObject bulletVariant;
    
}
