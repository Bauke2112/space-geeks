using UnityEngine;
using TimeUtils;

public class PauseHandler : MonoBehaviour {

  public string pauseKey;

  public bool isPaused {get; set;}
  public bool hasUpdated {get; set;} = false;

    private float lastTimeScale = 1f;

  private void Update() {
    TogglePause();

    if (hasUpdated) return;

    {
      if(isPaused) {
                lastTimeScale = Time.timeScale;
                TimeUtilities.PauseTime();
      } else {
                Time.timeScale = lastTimeScale;
      }

      Debug.Log("Time Scale: " + Time.timeScale);
      hasUpdated = true;
    }
  }

  private void TogglePause() {
    if(Input.GetButtonDown(pauseKey) || Input.GetButtonDown("button_9_joy_1") || Input.GetButtonDown("button_9_joy_2"))
        {
            
            hasUpdated = false;
      isPaused = !isPaused;
      AssetsHandler.i.pauseMenu.SetActive(isPaused);
    }
  }
}
