﻿using UnityEngine;

public class NoRotation : MonoBehaviour
{
    public Player trans;

    private void Update()
    {
        Vector3 pRot = new Vector3(trans.transform.rotation.x, trans.transform.rotation.y, trans.transform.rotation.z);
        this.transform.rotation = Quaternion.Euler(-1 * pRot);
    }
}
