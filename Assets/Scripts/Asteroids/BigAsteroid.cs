﻿using UnityEngine;

public class BigAsteroid : MonoBehaviour
{
    public Sprite[] sprites;

    private int index;

    void Start()
    {
        index = Random.Range(0, sprites.Length);
        this.GetComponent<SpriteRenderer>().sprite = sprites[index];
    }
}
