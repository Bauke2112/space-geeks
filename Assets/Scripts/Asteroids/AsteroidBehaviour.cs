﻿using UnityEngine;
using Atomic.Events;
using Timers;

public class AsteroidBehaviour : MonoBehaviour
{
    public float moveAngleMax = 360f;
    public float speed;
    public Vector3 size;
    public int minFragments = 2;
    public int maxFragments = 4;
    [Space]
    public float destroyTime = 10f;
    [Space]
    public float damage = .5f;
    [Space]
    public AsteroidType type;

    private Rigidbody2D rb;
    private Vector2 direction;

    private void Awake()
    {
        rb = this.GetComponent<Rigidbody2D>();
        if (rb == null) return;

        this.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, Random.Range(0f,moveAngleMax)));
        //this.transform.localScale = size;

        direction = this.transform.up;
    }

    int i;

    private void Start()
    {
        var force = direction * speed * rb.mass;
        rb.AddForce(force);

        i = Random.Range(1, 1000);

        Timer.CreateTimer(() =>
        {
            FragmentAsteroid(2);
        }, destroyTime, "Asteroid Death Timer" + i);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.CompareTag("Player"))
        {
            //Something happens when asteroid hits player
            CharacterStats stats = other.gameObject.GetComponent<CharacterStats>();
            if (stats == null) return;

            stats.TakeDamage(damage);

            FragmentAsteroid(Random.Range(minFragments, maxFragments));
            Timer.StopTimer("Asteroid Death Timer" + i);
            Destroy(this.gameObject, .000001f);
        }else if(other.gameObject.CompareTag("Enemy")) {
            CharacterStats stats = other.gameObject.GetComponent<CharacterStats>();
            if (stats == null) return;

            stats.TakeDamage(damage);

            FragmentAsteroid(Random.Range(minFragments, maxFragments));
            Timer.StopTimer("Asteroid Death Timer" + i);
            Destroy(this.gameObject, .000001f);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.CompareTag("Asteroid"))
        {
            //Something happens when two asteroids collide
            FragmentAsteroid(Random.Range(minFragments, maxFragments));
            Instantiate(AssetsHandler.i.destructionExplosion, new Vector3(this.transform.position.x,
                                                                          this.transform.position.y,
                                                                          0f), Quaternion.identity);
            Destroy(other.gameObject, .000001f);
        }else if(other.gameObject.CompareTag("Bullet"))
        {
            //Something happens when the asteroid hits the bullet
            FragmentAsteroid(Random.Range(minFragments, maxFragments));
            Instantiate(AssetsHandler.i.destructionExplosion, new Vector3(this.transform.position.x,
                                                                          this.transform.position.y,
                                                                          0f), Quaternion.identity);
            if(other.GetComponent<Bullet>().p.GetComponent<WeaponController>().atualWeapon.type == WeaponType.granadeiro)
            {
                Instantiate(other.GetComponent<Bullet>().expParticle, other.transform.position, Quaternion.identity);
            }
            EventManager.PublishEvent(new CustomEvent("Asteroid Destroyed"));
            Destroy(other.gameObject, .000001f);
        }
    }

    //Custom Functions
    private void FragmentAsteroid(int parts) {
        //List<GameObject> spawnedAsteroids = new List<GameObject>();

        if(this.type == AsteroidType.Big) {
            for(int i = 0; i < parts; i++) {
                float angle = Random.Range(0f,360f);

                GameObject go = Instantiate(AssetsHandler.i.mediumAsteroid, this.transform.position, Quaternion.Euler(new Vector3(0f,0f,angle)));
                //go.GetComponent<AsteroidBehaviour>().speed = 0;
                //go.GetComponent<Collider2D>().enabled = false;
                //spawnedAsteroids.Add(go);
            }
        }else if(this.type == AsteroidType.Medium) {
            for(int i = 0; i < parts; i++) {
                float angle = Random.Range(0f,360f);

                GameObject go = Instantiate(AssetsHandler.i.smallAsteroid, this.transform.position, Quaternion.Euler(new Vector3(0f,0f,angle)));
                //go.GetComponent<AsteroidBehaviour>().speed = 0;
                //go.GetComponent<Collider2D>().enabled = false;
                //spawnedAsteroids.Add(go);
            }
        }

        Timer.StopTimer("Asteroid Death Timer" + i);
        Destroy(this.gameObject);

        /*foreach(GameObject asteroid in spawnedAsteroids) {
            asteroid.GetComponent<Collider2D>().enabled = true;
            asteroid.GetComponent<AsteroidBehaviour>().speed = 200f;
        }*/
    }
}

public enum AsteroidType {
    Big,
    Medium,
    Small
}
