﻿using UnityEngine;

public class FollowerBullet : MonoBehaviour
{
    [SerializeField] float acc = 1000f;
    [SerializeField] float destroyTime = 5f;

    private Rigidbody2D rb;

    private void Start()
    {
        Destroy(gameObject, destroyTime);
        rb = GetComponent<Rigidbody2D>();
        rb.AddForce(transform.up * rb.mass * this.acc);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player") && !WeaponGlobal.active)
        {
            other.GetComponent<NewPlayerStats>().TakeDamage(1);
        }

        if(!other.CompareTag("Enemy")) Destroy(gameObject, .00000001f);
    }
}
