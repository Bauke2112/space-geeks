using UnityEngine;
using Effects;

public class Phantom : Enemy {
    public SpriteRenderer sp;
    public float ocilateSpeed;

    Vector3 centerLook;

    private void Start() {
        DecrAlpha("");
        centerLook = new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), 0);
    }

    public override void Update()
    {
        base.Update();
        if(target != null)
            transform.LookAt2D(target, 90f);
        else 
            transform.LookAt2D(centerLook, 90f);
    }

    void DecrAlpha(string s) {
        StartCoroutine(sp.DecreaseAlpha(ocilateSpeed, 0.2f, IcrAlpha));
    }
    void IcrAlpha(string s) {
        StartCoroutine(sp.IncreaseAlpha(ocilateSpeed, 1f, DecrAlpha));
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            collision.GetComponent<NewPlayerStats>().TakeDamage(1);
            this.GetComponent<CharacterStats>().TakeDamage(.5f);
        }
    }

    private void FixedUpdate() {
        if (target != null) {
            var direction = (target.position - transform.position).normalized;
            rb.AddForce(direction * speed * rb.mass);
        }
        else {
            var direction = (centerLook - transform.position).normalized;
            rb.AddForce(direction * speed * rb.mass);
        }
    }
}
