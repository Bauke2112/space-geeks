using UnityEngine;
using Timers;

public class Follower : Enemy {
  	public float stopDistance = 2f;

    [SerializeField] float fireRate = .7f;
    [SerializeField] float _fireAngle = .1f;

    private Vector2 direction;
  	private float startTime;
  	private float fixedSpeed;

    private bool canShoot = true;

    Vector3 centerLook;

    private Transform asteroid;

  	private void Start() {
    	startTime = Time.time;
    	fixedSpeed = speed * Time.fixedDeltaTime;
        centerLook = new Vector3(Random.Range(-2, 2), Random.Range(-2, 2), 0);
    }

  	public override void Update() {
        base.Update();
        if (transform.position.x < GameController.controller.left.position.x &&
           transform.position.x > GameController.controller.right.position.x &&
           transform.position.y > GameController.controller.up.position.y &&
           transform.position.y < GameController.controller.down.position.y)
            canShoot = false;

        if (target != null) {
            transform.LookAt2D(target, 90f);
            if (CheckTargetDistance(target) >= stopDistance + .5){}
            else
            {
                Shoot();
                if (target.gameObject.activeInHierarchy) canShoot = false; else canShoot = true;
            }
        } else {
            if (asteroid == null) transform.LookAt2D(centerLook, 90f);
            transform.LookAt2D(asteroid, 90f);
            Timer.CreateTimer(ShootAsteroid, .5f);
            canShoot = true;
        }
	}

    private void FixedUpdate() {
        if (target != null) {
            if (CheckTargetDistance(target) >= stopDistance)
                rb.AddForce(transform.up * speed * Time.fixedDeltaTime, ForceMode2D.Force);
        } else 
        {
            if (Vector2.Distance(centerLook, transform.position) >= stopDistance)
                rb.AddForce(transform.up * speed * Time.fixedDeltaTime, ForceMode2D.Force);
        }
  	}

	private float CheckTargetDistance(Transform target) {
    	return Vector2.Distance(target.position, transform.position);
  	}

    private void Shoot()
    {
        if(canShoot && target.gameObject.activeInHierarchy)
        {
            Vector3 fireDirection = (target.position + Random.insideUnitSphere * _fireAngle) - transform.position;
            canShoot = false;
            Timer.CreateTimer(() => canShoot = true, fireRate);
            Instantiate(AssetsHandler.i.enemyBullet, transform.position, Quaternion.LookRotation(Vector3.forward, fireDirection), null);

            var asteroids = GameObject.FindGameObjectsWithTag("Asteroid");
            asteroid = asteroids[Random.Range(0, asteroids.Length)].transform;
        }
    }

    private void ShootAsteroid()
    {
        Vector3 fireDirection = (asteroid.position + Random.insideUnitSphere * _fireAngle) - transform.position;
        Instantiate(AssetsHandler.i.enemyBullet, asteroid.position, Quaternion.LookRotation(Vector3.forward, fireDirection), null);
    }
}
