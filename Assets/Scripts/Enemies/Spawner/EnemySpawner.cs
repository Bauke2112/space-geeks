﻿using UnityEngine;
using Timers;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour
{
    public static EnemySpawner i;

    private void Awake()
    {
        if (!i) i = this;
        else
        {
            Destroy(i.gameObject);
            i = this;
        }
    }
    public float timeToSpawn;
    public float range;

    public static List<GameObject> enemyList = new List<GameObject>();

    public bool waveSystem = true;

    [SerializeField]private bool canSpawn = true;

    private void Start()
    {
        if(!waveSystem) Timer.CreateTimer(SpawnOverTime, 3f);
    }

    private void SpawnOverTime() {
        var pos = GeneratePosition(AssetsHandler.i.players[0], AssetsHandler.i.players[1]);
        var enemyToSpawn = AssetsHandler.i.enemies[Random.Range(0, AssetsHandler.i.enemies.Length)];

        Instantiate(enemyToSpawn, pos, Quaternion.identity, null);
        enemyList.Add(enemyToSpawn);
        Timer.CreateTimer(SpawnOverTime, timeToSpawn);
    }

    public void Spawn(int enemiesToSpawn, float time)
    {
        for(int i = 1; i <= enemiesToSpawn; i++)
        {
            Timer.CreateTimer(SpawnSingleEnemy, time * i);
        }
    }

    private void SpawnSingleEnemy()
    {
        var pos = GeneratePosition(AssetsHandler.i.players[0], AssetsHandler.i.players[1]);
        var enemyToSpawn = AssetsHandler.i.enemies[Random.Range(0, AssetsHandler.i.enemies.Length)];

        Instantiate(enemyToSpawn, pos, Quaternion.identity, null);
    }

    private Vector2 GeneratePosition(Player one, Player two) {
        float x, y;
        x = Random.Range(-range, range);
        y = Random.Range(-range, range);

        var dist_one = Vector2.Distance(one.transform.position, new Vector2(x, y));
        var dist_two = Vector2.Distance(two.transform.position, new Vector2(x, y));

        if(dist_one < one.safeZoneRadius || dist_two < two.safeZoneRadius) {
            GeneratePosition(one, two);
            return Vector2.zero;
        }

        return new Vector2(x, y);
    }
}
