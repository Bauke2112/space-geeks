﻿using UnityEngine;
using Timers;
using Atomic.Events;

public class WaveSystem : MonoBehaviour, IEventObject
{
    public Wave[] waves;

    [SerializeField] private EnemySpawner spawner;

    private Wave curWave;
    private int curWaveNumber = 0;
    private int curRound = 1;

    private int enemySpawnCount;
    private int curEnemyCount;

    private void Start()
    { 
        if (spawner == null) Debug.Log("Spawner can't be null");
        if (!spawner.waveSystem) return;
        EventManager.SubscribeToEvent("Enemy Destroyed Waves", this);

        NextWave();
    }

    private void OnDisable()
    {
        if (!spawner.waveSystem) return;
        EventManager.Unsubscribe("Enemy Destroyed Waves");
    }

    private void NextWave()
    {
        if(curWave == waves[waves.Length - 1])
        {
            curWaveNumber = 1;
            curRound++;
        }else
        {
            curWaveNumber++;
        }

        Debug.Log("Wave: " + curWaveNumber + " Round: " + curRound);

        curWave = waves[curWaveNumber - 1];

        enemySpawnCount = curWave.enemyCount + (curRound * 2);
        curEnemyCount = enemySpawnCount;
        spawner.Spawn(enemySpawnCount, curWave.timeToSpawn);
    }

    public void OnEventReceived(CustomEvent _e)
    {
        foreach (string s in EventManager.subscribers.keys)
        {
            if (s == _e.name && EventManager.subscribers.FindValue(s) == this)
            {
                curEnemyCount -= 1;

                if(curEnemyCount == 0)
                {
                    NextWave();
                }
                break;
            }
        }
    }
}
