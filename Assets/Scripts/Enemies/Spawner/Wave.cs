﻿[System.Serializable]
public class Wave
{
    public string name;
    //public WaveType type;
    public int enemyCount;
    public float timeToSpawn = 4f;
}

public enum WaveType
{
    Enemy,
    Boss
}
