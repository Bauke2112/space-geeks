﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Enemy : MonoBehaviour {
    //References
    public CharacterStats stats {get; set;}
    public Rigidbody2D rb {get; set;}

    public float speed;

    public Transform target {get; set;}

    private void Awake() {
        stats = this.GetComponent<CharacterStats>();
        rb = this.GetComponent<Rigidbody2D>();

        if(target == null) GetTarget();
    }

    public virtual void Update() {
        if (target == null) {
            GetOtherTarget();
        }
        else {
            if (!target.gameObject.activeInHierarchy) {
                GetOtherTarget();
            }
        }
    }

    private void GetTarget() {
        List<Player> possibleTargets = AssetsHandler.i.players.ToList();
        if(possibleTargets == null) return;

        target = possibleTargets[Random.Range(0,possibleTargets.Count)].transform;
    }

    public void GetOtherTarget() {
        List<Player> possibleTargets = AssetsHandler.i.players.ToList();
        if (possibleTargets == null) return;
        if (target == null) {
            //target = possibleTargets[Random.Range(0, possibleTargets.Count)].transform;
            if (possibleTargets[1].gameObject.activeInHierarchy)
                target = possibleTargets[1].transform;
            else if (possibleTargets[0].gameObject.activeInHierarchy)
                target = possibleTargets[0].transform;
      
        } else {
            var index = possibleTargets.IndexOf(target.gameObject.GetComponent<Player>());
            if (index == 0 && possibleTargets[1].gameObject.activeInHierarchy)
                target = possibleTargets[1].transform;
            else if (index == 1 && possibleTargets[0].gameObject.activeInHierarchy)
                target = possibleTargets[0].transform;
            else {
                target = null;
                //target.position = new Vector3(Random.Range(-1, 1), Random.Range(-1, 1), 0);
            }
        }
    }
}
