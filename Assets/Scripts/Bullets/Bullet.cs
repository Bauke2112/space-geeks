﻿using UnityEngine;
using System.Collections.Generic;

public class Bullet : MonoBehaviour
{
    private Rigidbody2D rb;
    public float acc = 10;
    
    [HideInInspector]
    public float damage;
    [HideInInspector]
    public float energy;

    public float timeToDestroy;

    public Player p { get; set; }

    public static Player player;
    private Player pl;
    private AudioSource source;
    private GameObject[] enemyList;
    public GameObject expParticle;

    private void Awake()
    {
        rb = this.GetComponent<Rigidbody2D>();
        source = this.GetComponent<AudioSource>();
    }

    public void Update()
    {
        if (p.GetComponent<WeaponController>().atualWeapon.type == WeaponType.phantomBullet)
        {
            enemyList = GameObject.FindGameObjectsWithTag("Enemy");
            GameObject nearstEnemy = findNearEnemy(enemyList);
            var dir = (transform.position - nearstEnemy.transform.position).normalized;
            //var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
           // t.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle));
            transform.LookAt2D(nearstEnemy.transform.position, 0);
            rb.velocity = new Vector2(0,0);
            rb.AddForce(-dir * rb.mass * acc);
        }
    }

    private void Start()
    {
        pl = player;
        source.clip = AssetsHandler.i.bullet;
        source.Play();
        rb.AddForce(transform.up * rb.mass * acc);
        Destroy(gameObject, timeToDestroy);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.gameObject.CompareTag("Enemy")) {
            CharacterStats stats = other.GetComponent<CharacterStats>();
            stats.TakeDamage(damage);
            Debug.Log("Enemy Lives: " + stats.lives);
            if (p.GetComponent<WeaponController>().atualWeapon.type == WeaponType.granadeiro)
            {
                Instantiate(expParticle, transform.position, Quaternion.identity);
            }
            StartCoroutine(Camera.main.GetComponent<CameraShake>().Shake(.1f, .35f));
            Destroy(this.gameObject, .00001f);  
        }
        if(other.CompareTag("Bullet")){
            Debug.Log("Colidiu");
            Instantiate(expParticle, transform.position, Quaternion.identity);
            Destroy(other.gameObject, 0.0001f);
            Destroy(gameObject, .00001f);
        }    
    }

    GameObject near;
    public GameObject findNearEnemy(GameObject[] enemys)
    {
        
        List<float> d = new List<float>();
        var min = float.MaxValue;
        for (int i = 0;i < enemys.Length; i++)
        {
            d.Add(Vector2.Distance(transform.position, enemys[i].transform.position));
            if(d[i] < min)
            {
                min = d[i];
                near = enemys[i];
            }
        }
        return near;
    }
}
