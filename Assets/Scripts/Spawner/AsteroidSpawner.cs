﻿using UnityEngine;
using Timers;
using System.Collections.Generic;

public class AsteroidSpawner : MonoBehaviour
{
    [Header("Spawn Area")]
    public float spawnRadius;
    public Transform left, right, up, down;

    [Header("Time Variables")]
    public float timeToSpawn;

    private Timer timer;

    public List<GameObject> curAsteroids = new List<GameObject>();

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(this.transform.position, new Vector2(2 * spawnRadius, 2 * spawnRadius));
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(this.transform.position, spawnRadius);
    }

    private void Start()
    {
        Timer.CreateTimer(Spawn, 2f);
    }

    private Vector3 ChooseSpawnPos()
    {
        float rand = Random.Range(-1f,1f);
        float side = Random.Range(-1f, 1f);

        if (side > 0) //up-down
        {
            if (rand < 0) //up
            {
                return new Vector3(up.position.x, up.position.y + 2f);
            }
            //down
            return new Vector3(down.position.x, down.position.y - 2f);
        }
        else if (side < 0) //right-left
        {
            if (rand < 0) //right
            {
                return new Vector3(right.position.x + 2f, right.position.y);
            }
            //left
            return new Vector3(left.position.x - 2f, left.position.y);
        }

        return new Vector3(99999999, 99999999);
    }

    private void Spawn()
    {
        var rot = new Vector3(0f, 0f, 0f);
        //var pos = GeneratePosition(AssetsHandler.i.players[0], AssetsHandler.i.players[1]);
        var pos = ChooseSpawnPos(); 

        GameObject asteroid = Instantiate(AssetsHandler.i.bigAsteroid, pos, Quaternion.Euler(rot), this.transform);
        curAsteroids.Add(asteroid);
        asteroid.transform.LookAt2D(Vector2.zero, 0f);
        asteroid.transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, transform.rotation.z + Random.Range(-45f, 45f)));

        timer = Timer.CreateTimer(Spawn, timeToSpawn, "Spawn Asteroids Timer");
        Debug.Log("Spawned!");
    }

    private Vector2 GeneratePosition(Player one, Player two) {
        float x, y;
        x = Random.Range(-spawnRadius, spawnRadius);
        y = Random.Range(-spawnRadius, spawnRadius);

        var dist_one = Vector2.Distance(one.transform.position, new Vector2(x, y));
        var dist_two = Vector2.Distance(two.transform.position, new Vector2(x, y));

        if (dist_one < one.safeZoneRadius || dist_two < two.safeZoneRadius) {
            GeneratePosition(one, two);
            return Vector2.zero;
        }

        return new Vector2(x, y);
    }
}
