﻿using UnityEngine;
using Buttons;

public class MainMenu : MonoBehaviour {

    public void Load(string name)
    {
        ButtonsUtilities.LoadScene(name);
    }

    public void TransitionLoad(string name) {
        //ButtonsUtilities.LoadScene(name);
        Transition.transition.FadeIn(ButtonsUtilities.LoadScene, name);
    }

    public void QuitGame() {
        ButtonsUtilities.Exit();
    }
}
