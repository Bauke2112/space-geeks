﻿using UnityEngine;
using TMPro;
using Timers;

public class DisplayPlayerStats : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI playerOneLifeText;
    [SerializeField] TextMeshProUGUI playerTwoLifeText;

    [SerializeField] Int playerOneLife;
    [SerializeField] Int playerTwoLife;

    [SerializeField] TextMeshProUGUI timerOne;
    [SerializeField] TextMeshProUGUI timerTwo;

    private void Update()
    {
        playerOneLifeText.text = "x" + playerOneLife.value.ToString();
        playerTwoLifeText.text = "x" + playerTwoLife.value.ToString();

        timerOne.transform.position = AssetsHandler.i.players[0].transform.position;
        timerTwo.transform.position = AssetsHandler.i.players[1].transform.position;

        timerOne.text = "Time to respawn: " + Timer.GetTimerTimeFloat("Player 1 Respawn Timer").ToString("F1");
        timerTwo.text = "Time to respawn: " + Timer.GetTimerTimeFloat("Player 2 Respawn Timer").ToString("F1");
    }
}
