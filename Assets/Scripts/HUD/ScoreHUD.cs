﻿using TMPro;
using UnityEngine;

public class ScoreHUD : MonoBehaviour
{
    public TextMeshProUGUI score;
    public Int scoreObject;

    private void Update()
    {
        score.text = scoreObject.value.ToString();
    }
}
