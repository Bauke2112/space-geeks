﻿using UnityEngine;
using Buttons;
public class PauseMenu : MonoBehaviour {
    public PauseHandler pauseHandler;
    public void LoadScene(string name) {
        ButtonsUtilities.LoadScene(name);
    }

    public void TransitionLoad(string name) {
        Resume();
        Transition.transition.FadeIn(ButtonsUtilities.LoadScene, name);
    }

    public void QuitGame() {
        ButtonsUtilities.Exit();
    }

    public void Resume() {
        pauseHandler.isPaused = false;
        pauseHandler.hasUpdated = false;
        AssetsHandler.i.pauseMenu.SetActive(false);
    }
}
