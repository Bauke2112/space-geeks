﻿using TMPro;
using UnityEngine;

public class GameOverHUD : MonoBehaviour
{
    public TextMeshProUGUI score, highScore;

    private void Update()
    {
        score.text = PlayerPrefs.GetInt("score").ToString();
        highScore.text = PlayerPrefs.GetInt("high-score").ToString();
    }
}
