﻿using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {
    public static GameController controller;

    private void Awake()
    {
        if (!controller) controller = this;
        else
        {
            Destroy(controller.gameObject);
            controller = this;
        }
    }

    public TextMeshProUGUI timeText;

    private float startTime;
    private float timer;

    public Transform right, left, up, down;
    public Vector3[] screenLimit;
    public Vector3 topRightCorner;

    private void Start() {
        SceneManager.sceneLoaded += OnSceneLoaded;

        Vector3 limit = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f));
        right.transform.position = new Vector3(limit.x, limit.y/2);
        left.transform.position = new Vector3(-limit.x, limit.y / 2);
        up.transform.position = new Vector3(limit.x / 2, limit.y);
        down.transform.position = new Vector3(limit.x / 2, -limit.y);

        screenLimit = new Vector3[4]
        {
            right.transform.position,
            left.transform.position,
            up.transform.position,
            down.transform.position
        };

        topRightCorner = limit;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == "Game" || scene.name == "Game_test")
            startTime = Time.time;

        timer = startTime;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }

    private void Update() {
        timer += Time.deltaTime;
        timeText.text = timer.ToString("F1") + "s";
    }
}
