﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterStatsHUD : CharacterStats {
    public Image[] healthIcons;

    private void Update() {
        switch(this.lives) {
            case 6:
                healthIcons[0].fillAmount = 1;
                healthIcons[1].fillAmount = 1;
                healthIcons[2].fillAmount = 1;
                break;
            case 5:
                healthIcons[0].fillAmount = 1;
                healthIcons[1].fillAmount = 1;
                healthIcons[2].fillAmount = .457f;
                break;
            case 4:
                healthIcons[0].fillAmount = 1;
                healthIcons[1].fillAmount = 1;
                healthIcons[2].fillAmount = 0;
                break;
            case 3:
                healthIcons[0].fillAmount = 1;
                healthIcons[1].fillAmount = .457f;
                healthIcons[2].fillAmount = 0;
                break;
            case 2:
                healthIcons[0].fillAmount = 1;
                healthIcons[1].fillAmount = 0;
                healthIcons[2].fillAmount = 0;
                break;
            case 1:
                healthIcons[0].fillAmount = .457f;
                healthIcons[1].fillAmount = 0;
                healthIcons[2].fillAmount = 0;
                break;
            case 0:
                healthIcons[0].fillAmount = 0;
                healthIcons[1].fillAmount = 0;
                healthIcons[2].fillAmount = 0;
                break;
            default:
                healthIcons[0].fillAmount = 0;
                healthIcons[1].fillAmount = 0;
                healthIcons[2].fillAmount = 0;
                break;
        }
    }
}
