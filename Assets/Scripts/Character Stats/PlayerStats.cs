﻿using UnityEngine;
using Timers;
using Atomic.Events;

public class PlayerStats : CharacterStats {

    public float shieldActivationTime = 5f;
    private bool shieldActive = true;

    public int respawnTimes = 3;
    public GameObject shield;

    public GameObject[] lifeIcons;

    private const float RESPAWN_TIME = 5f;
    private bool hasUpdated;

    private Player player;

    private void Start() {
        this.maxLives = 2;
        this.lives = 2;

        player = GetComponent<Player>();
    }

    private void Update() {
        switch(lives) {
            case 2:
                shieldActive = true;
                break;
            case 1:
                shieldActive = false;
                break;
            case 0:
                shieldActive = false;
                break;
            default:
                shieldActive = true;
                break;
        }

        switch(respawnTimes) {
            case 3:
                lifeIcons[0].SetActive(true);
                lifeIcons[1].SetActive(true);
                lifeIcons[2].SetActive(true);
                break;
            case 2:
                lifeIcons[0].SetActive(false);
                lifeIcons[1].SetActive(true);
                lifeIcons[2].SetActive(true);
                break;
            case 1:
                lifeIcons[0].SetActive(false);
                lifeIcons[1].SetActive(false);
                lifeIcons[2].SetActive(true);
                break;
            case 0:
                lifeIcons[0].SetActive(false);
                lifeIcons[1].SetActive(false);
                lifeIcons[2].SetActive(false);
                break;
        }

        if(Input.GetKeyDown(KeyCode.T)) {
            this.TakeDamage(1);
        }

        if(!shieldActive) {
            shield.SetActive(false);
            if(hasUpdated == false) {
                hasUpdated = true;
                Timer.CreateTimer(() => {
                    shieldActive = true;
                    this.Heal(1f);
                    hasUpdated = false;
                }, shieldActivationTime);
            }
        }else {
            shield.SetActive(true);
        }

        if (respawnTimes == 0)
        {
            if (player.playerNumber == 1)
            {
                PlayerDeathController.controller.oneHasDied = true;
                PlayerDeathController.controller.one = player;
            }

            if (player.playerNumber == 2)
            {
                PlayerDeathController.controller.twoHasDied = true;
                PlayerDeathController.controller.two = player;
            }
        }
    }

    public override void Die() {
        if(lives <= 0) {
            Debug.Log("You have died!");

            this.SetActive(false);
            Timer.CreateTimer(() => {
                this.SetActive(true);
                respawnTimes -= 1;
            }, RESPAWN_TIME);
        }
    }
}
