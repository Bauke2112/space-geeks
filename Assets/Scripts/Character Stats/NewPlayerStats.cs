﻿using UnityEngine;
using Timers;
using TMPro;

public class NewPlayerStats : CharacterStats
{
    [SerializeField] int livesCount;
    [SerializeField] new Int lives;

    [SerializeField] GameObject shield;
    [SerializeField] float shieldActivationTime = 5f;
    [SerializeField] float respawnTime = 5f;

    [SerializeField] TextMeshProUGUI timer;

    [SerializeField] GameObject deathEffect;
    [SerializeField] bool godMode = false;

    private bool shieldActive = true;

    private delegate void ShieldHandler(bool state);
    private ShieldHandler onShieldStateChanged;

    private Player player;

    private new void Awake()
    {
        lives.value = livesCount;
        onShieldStateChanged += OnShieldUpdates;

        player = GetComponent<Player>();
    }

    private void OnShieldUpdates(bool state)
    {
        shield.SetActive(state);
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.T))
        {
            TakeDamage(1f);
        }
    }

    public void LifeUp()
    {
        lives.value++;
    }

    public override void TakeDamage(float damage)
    {
        if (godMode) return;
        if (isInvensible) return;

        if(shieldActive)
        {
            shieldActive = false;

            onShieldStateChanged?.Invoke(shieldActive);

            Timer.CreateTimer(() => 
            {
                shieldActive = true;

                onShieldStateChanged?.Invoke(shieldActive);
            }, shieldActivationTime);
        }
        else
        {
            lives.value -= 1;
            Die();
        }
    }

    public override void Die()
    {   
        if(lives.value > 0)
        {
            gameObject.SetActive(false);
            timer.SetActive(true);

            Timer.CreateTimer(() =>
            {
                timer.SetActive(false);

                gameObject.SetActive(true);
                gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
                shieldActive = true;

                onShieldStateChanged?.Invoke(shieldActive);
            }, respawnTime, "Player " + player.playerNumber + " Respawn Timer");
        }else
        {
            Instantiate(deathEffect, transform.position, transform.rotation, null);
            gameObject.SetActive(false);
           // Destroy(gameObject, .0000000001f);

            if (player.playerNumber == 1)
            {
                PlayerDeathController.controller.oneHasDied = true;
                PlayerDeathController.controller.one = player;
            }

            if (player.playerNumber == 2)
            {
                PlayerDeathController.controller.twoHasDied = true;
                PlayerDeathController.controller.two = player;
            }
        }
    }
}
