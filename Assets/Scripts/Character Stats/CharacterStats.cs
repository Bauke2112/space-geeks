﻿using UnityEngine;
using Atomic.Events;

public class CharacterStats : MonoBehaviour {
    public float lives { get; set; }
    public float maxLives;

    public bool isInvensible {get; set;} = false;
    public bool hasShield {get; set;} = false;

    public float damageReduction = .5f;

    public void Awake()
    {
        lives = maxLives;
        isInvensible = false;
        hasShield = false;
    }

    public virtual float GetFillAmount() {
        return (lives / maxLives);
    }

    public virtual void TakeDamage(float damage) {
        if(this.isInvensible) return;

        if(!hasShield) this.lives -= damage;
        else if(hasShield) this.lives -= damage - damageReduction;

        //this.lives -= damage;
         
        Die();
        Debug.Log(this.gameObject.name + " " + damage);
    }

    public virtual void Heal(float energyAmount) {
        if(this.lives + energyAmount > maxLives) {
            lives += maxLives - lives;
        }else {
            this.lives += energyAmount;
        }
    }

    public virtual void Die() {
        if(lives <= 0) {
            Debug.Log("You have died!");
            EventManager.PublishEvent(new CustomEvent("Enemy Destroyed"));

            Destroy(this.gameObject, .0001f);
        }
    }
}
