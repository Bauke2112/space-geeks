﻿using UnityEngine;
using Timers;

public class MainMenuEffect : MonoBehaviour {
    public GameObject asteroid;
    public GameObject player;
    public GameObject bullet;

    private Vector3 direction;

    private void Start() {
        direction = asteroid.transform.right * 2 * Time.deltaTime;
    }

    private void Update() {
        asteroid.transform.Translate(direction);
        asteroid.transform.GetChild(0).Rotate(new Vector3(0f,0f,20f * Time.deltaTime));
    }
}
