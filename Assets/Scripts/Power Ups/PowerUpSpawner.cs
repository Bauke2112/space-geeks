﻿using System.Collections.Generic;
using UnityEngine;
using Timers;

public class PowerUpSpawner : MonoBehaviour
{
    public List<PowerUp> powerUps = new List<PowerUp>();
    public float radius = 10;
    public float timeToSpawn = 10f;

    public PowerUpInstance item;

    private void Start()
    {
        Spawn();
    }

    private void Spawn()
    {
        var pos = new Vector2(Random.Range(-radius, radius), Random.Range(-radius, radius));
        PowerUpInstance _item = Instantiate(item, pos, Quaternion.identity);
        _item.power = powerUps[Random.Range(0, powerUps.Count)];

        Timer.CreateTimer(Spawn, timeToSpawn);
    }
}
