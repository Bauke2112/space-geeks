﻿using UnityEngine;

[CreateAssetMenu(fileName = "Power Up", menuName = "Space Geeks/Power Up", order = 1)]
public class PowerUp : ScriptableObject
{
    public new string name;
    public float duration;
    public float useValue;
    public PowerUpType type;
    [Space]
    public Sprite icon;
    public Sprite effect;
    public Color effectColor;
    [Space]
    public Vector3 size = new Vector3(2,2,2);
}

public enum PowerUpType
{
    Shield,
    Speed,
    Damage,
    Life,
    Star,
    PEM
}
