﻿using UnityEngine;

[RequireComponent(typeof(CircleCollider2D), typeof(SpriteRenderer))]
public class PowerUpInstance : MonoBehaviour
{
    public PowerUp power;
    public float radius;

    private void Start()
    {
        Destroy(this.gameObject, 15f);
        this.GetComponent<CircleCollider2D>().radius = radius;
        this.GetComponent<SpriteRenderer>().sprite = power.icon;
        transform.localScale = power.size;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            //Get PowerUpController from player
            PowerUpController controller = collision.GetComponent<PowerUpController>();

            if (controller == null) return;

            if (controller.power != null) return;

            controller.UsePowerUp(power);
            Destroy(this.gameObject, .0000001f);
        }
    }
}
