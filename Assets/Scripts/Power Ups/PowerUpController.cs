﻿using UnityEngine;
using Timers;
using Atomic.Events;

[RequireComponent(typeof(CharacterStats), typeof(Player))]
public class PowerUpController : MonoBehaviour
{
    public PowerUp power { get; set; }

    private NewPlayerStats newplayerStats;
    private PlayerStats playerStats;
    private Player player;
    [SerializeField]private SpriteRenderer effectRenderer;

    private void Awake()
    {
        newplayerStats = this.GetComponent<NewPlayerStats>();
        playerStats = this.GetComponent<PlayerStats>();
        player = this.GetComponent<Player>();

        if (effectRenderer == null) Debug.LogError("Effect Renderer can't be null");
    }

    public void UsePowerUp(PowerUp powerUp)
    {
        var returnTo = .0f;
        power = powerUp;
        EventManager.PublishEvent(new CustomEvent("Power Up"));

        switch(powerUp.type)
        {
            case PowerUpType.Damage:
                player.GetComponent<WeaponController>().atualWeapon.damage += powerUp.useValue;

                Timer.CreateTimer(() =>
                {
                    player.GetComponent<WeaponController>().atualWeapon.damage = player.GetComponent<WeaponController>().atualWeapon.baseDamage;
                    power = null;
                }, powerUp.duration, "Power Up Timer");
                break;

            case PowerUpType.Life:
                if(newplayerStats != null) newplayerStats.LifeUp();
                power = null;
                break;

            case PowerUpType.PEM:
                break;

            case PowerUpType.Shield:
                break;

            case PowerUpType.Speed:
                returnTo = player.maxSpeed;
                player.maxSpeed += powerUp.useValue;

                Timer.CreateTimer(() => {
                    power = null;
                    player.maxSpeed = returnTo;
                }, powerUp.duration, "Power Up Timer");
                break;

            case PowerUpType.Star:
                _ = playerStats != null ? playerStats.isInvensible = true : newplayerStats.isInvensible = true;
                effectRenderer.sprite = powerUp.effect;
                effectRenderer.color = powerUp.effectColor;

                Timer.CreateTimer(() =>
                {
                    power = null;
                    _ = playerStats != null ? playerStats.isInvensible = false : newplayerStats.isInvensible = false;
                    effectRenderer.sprite = null;
                }, powerUp.duration, "Power Up Timer");
                break;
        }
    }
}
